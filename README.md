# README #

To setup this repository, please read below documentation carefully. Its not that hard though.

### What is this repository for? ###

* This was a project initially developed by me. I am not the designer, so user interface credit goes to someone else. I only did the coding, database designing & architecture defining part.
* Its a fully completed custom made eCommerce website. All you need to do is just execute the .sql file, config the connection and its ready to go.



### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* You can email me if you have any questions at: mijo.jfwg@gmail.com